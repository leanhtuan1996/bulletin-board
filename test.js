import Vue from 'vue'
import Router from 'vue-router'

const Dashboard = () => import('@/components/Dashboard')
const OrdersTrace = () => import('@/components/orders/OrdersTrace')
const Products = () => import('@/components/inventories/Products')

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/orders/trace',
      name: 'Orders',
      component: OrdersTrace
    },
    {
      path: '/products',
      name: 'Products',
      component: Products
    }
  ]
})
